import * as tape from 'tape';
import {MockEnvironment} from "../../../../../../../mock/environment";
import {MockLoader} from "../../../../../../../mock/loader";
import {escape} from "../../../../../../../../src/lib/extension/core/filters/escape";
import {TwingEnvironment} from "../../../../../../../../src/lib/environment";
import {MockTemplate} from "../../../../../../../mock/template";

function foo_escaper_for_test(env: TwingEnvironment, string: string, charset: string) {
    return (string ? string : '') + charset;
}

/**
 * All character encodings supported by htmlspecialchars().
 */
let htmlSpecialChars: { [k: string]: string } = {
    '\'': '&#039;',
    '"': '&quot;',
    '<': '&lt;',
    '>': '&gt;',
    '&': '&amp;',
};

let htmlAttrSpecialChars: { [k: string]: string | [string, string] } = {
    '\'': '&#x27;',
    /* Characters beyond ASCII value 255 to unicode escape */
    'Ā': '&#x0100;',
    '😀': '&#x1F600;',
    /* Immune chars excluded */
    ',': ',',
    '.': '.',
    '-': '-',
    '_': '_',
    /* Basic alnums excluded */
    'a': 'a',
    'A': 'A',
    'z': 'z',
    'Z': 'Z',
    '0': '0',
    '9': '9',
    /* Basic control characters and null */
    '[return]': ["\r", '&#x0D;'],
    '[newline]': ["\n", '&#x0A;'],
    '[tab]': ["\t", '&#x09;'],
    '[null]': ["\0", '&#xFFFD;'], // should use Unicode replacement char
    /* Encode chars as named entities where possible */
    '<': '&lt;',
    '>': '&gt;',
    '&': '&amp;',
    '"': '&quot;',
    /* Encode spaces for quoteless attribute protection */
    '[space]': [' ', '&#x20;'],
};

let jsSpecialChars: { [k: string]: string | [string, string] } = {
    /* HTML special chars - escape without exception to hex */
    '<': '\\u003C',
    '>': '\\u003E',
    '\'': '\\u0027',
    '"': '\\u0022',
    '&': '\\u0026',
    '/': '\\/',
    /* Characters beyond ASCII value 255 to unicode escape */
    'Ā': '\\u0100',
    '😀': '\\uD83D\\uDE00',
    /* Immune chars excluded */
    ',': ',',
    '.': '.',
    '_': '_',
    /* Basic alnums excluded */
    'a': 'a',
    'A': 'A',
    'z': 'z',
    'Z': 'Z',
    '0': '0',
    '9': '9',
    /* Basic control characters and null */
    '[return]': ["\r", '\\r'],
    '[newline]': ["\n", '\\n'],
    '[backspace]': ["\x08", '\\b'],
    '[tab]': ["\t", '\\t'],
    '[formfeed]': ["\x0C", '\\f'],
    '[null]': ["\0", '\\u0000'],
    /* Encode spaces for quoteless attribute protection */
    '[space]': [' ', '\\u0020'],
};

let urlSpecialChars: { [k: string]: string | [string, string] } = {
    /* HTML special chars - escape without exception to percent encoding */
    '<': '%3C',
    '>': '%3E',
    '\'': '%27',
    '"': '%22',
    '&': '%26',
    /* Characters beyond ASCII value 255 to hex sequence */
    'Ā': '%C4%80',
    /* Punctuation and unreserved check */
    ',': '%2C',
    '.': '.',
    '_': '_',
    '-': '-',
    ':': '%3A',
    ';': '%3B',
    '!': '%21',
    /* Basic alnums excluded */
    'a': 'a',
    'A': 'A',
    'z': 'z',
    'Z': 'Z',
    '0': '0',
    '9': '9',
    /* Basic control characters and null */
    '[return]': ["\r", '%0D'],
    '[newline]': ["\n", '%0A'],
    '[tab]': ["\t", '%09'],
    '[null]': ["\0", '%00'],
    /* PHP quirks from the past */
    '[space]': [' ', '%20'],
    '~': '~',
    '+': '%2B',
};

let cssSpecialChars: { [k: string]: string | [string, string] } = {
    /* HTML special chars - escape without exception to hex */
    '<': '\\3C ',
    '>': '\\3E ',
    '\'': '\\27 ',
    '"': '\\22 ',
    '&': '\\26 ',
    /* Characters beyond ASCII value 255 to unicode escape */
    'Ā': '\\100 ',
    /* Immune chars excluded */
    ',': '\\2C ',
    '.': '\\2E ',
    '_': '\\5F ',
    /* Basic alnums excluded */
    'a': 'a',
    'A': 'A',
    'z': 'z',
    'Z': 'Z',
    '0': '0',
    '9': '9',
    /* Basic control characters and null */
    '[return]': ["\r", '\\D '],
    '[newline]': ["\n", '\\A '],
    '[tab]': ["\t", '\\9 '],
    '[null]': ["\0", '\\0 '],
    /* Encode spaces for quoteless attribute protection */
    '[space]': [' ', '\\20 '],
};

/**
 * Convert a Unicode Codepoint to a literal UTF-8 character.
 *
 * @param {number} codepoint Unicode codepoint in hex notation
 *
 * @return string UTF-8 literal string
 */
let codepointToUtf8 = function (codepoint: number) {
    if (codepoint < 0x110000) {
        return String.fromCharCode(codepoint);
    }

    throw new Error('Codepoint requested outside of Unicode range.');
};

let getTemplate = function () {
    return new MockTemplate(new MockEnvironment(new MockLoader()));
};

tape('escaping', (test) => {
    test.test('htmlEscapingConvertsSpecialChars', async (test) => {
        for (let key in htmlSpecialChars) {
            let keyName = key;
            let value = htmlSpecialChars[key];
            if (Array.isArray(value)) {
                key = value[0];
                value = value[1];
            }

            test.same(await escape(getTemplate(), key, 'html'), value, 'Succeed at escaping: ' + keyName);
        }

        test.end();
    });

    test.test('htmlAttributeEscapingConvertsSpecialChars', async (test) => {
        for (let key in htmlAttrSpecialChars) {
            let keyName = key;
            let value = htmlAttrSpecialChars[key];
            if (Array.isArray(value)) {
                key = value[0];
                value = value[1];
            }

            test.same(await escape(getTemplate(), key, 'html_attr'), value, 'Succeed at escaping: ' + keyName);
        }

        test.end();
    });

    test.test('javascriptEscapingConvertsSpecialChars', async (test) => {
        for (let key in jsSpecialChars) {
            let keyName = key;
            let value = jsSpecialChars[key];
            if (Array.isArray(value)) {
                key = value[0];
                value = value[1];
            }

            test.same(await escape(getTemplate(), key, 'js'), value, 'Succeed at escaping: ' + keyName);
        }

        test.end();
    });

    test.test('javascriptEscapingReturnsStringIfZeroLength', async (test) => {
        test.same(await escape(getTemplate(), '', 'js'), '', 'Succeed at escaping: ""');

        test.end();
    });

    test.test('javascriptEscapingReturnsStringIfContainsOnlyDigits', async (test) => {
        test.same(await escape(getTemplate(), '123', 'js'), '123', 'Succeed at escaping: "123"');

        test.end();
    });

    test.test('cssEscapingConvertsSpecialChars', async (test) => {
        for (let key in cssSpecialChars) {
            let keyName = key;
            let value = cssSpecialChars[key];
            if (Array.isArray(value)) {
                key = value[0];
                value = value[1];
            }

            test.same(await escape(getTemplate(), key, 'css'), value, 'Succeed at escaping: ' + keyName);
        }

        test.end();
    });

    test.test('cssEscapingReturnsStringIfZeroLength', async (test) => {
        test.same(await escape(getTemplate(), '', 'css'), '', 'Succeed at escaping: ""');

        test.end();
    });

    test.test('cssEscapingReturnsStringIfContainsOnlyDigits', async (test) => {
        test.same(await escape(getTemplate(), '123', 'css'), '123', 'Succeed at escaping: "123"');

        test.end();
    });

    test.test('urlEscapingConvertsSpecialChars', async (test) => {
        for (let key in urlSpecialChars) {
            let keyName = key;
            let value = urlSpecialChars[key];
            if (Array.isArray(value)) {
                key = value[0];
                value = value[1];
            }

            test.same(await escape(getTemplate(), key, 'url'), value, 'Succeed at escaping: ' + keyName);
        }

        test.end();
    });

    /**
     * Range tests to confirm escaped range of characters is within OWASP recommendation.
     */

    /**
     * Only testing the first few 2 ranges on this prot. function as that's all these
     * other range tests require.
     */
    test.test('unicodeCodepointConversionToUtf8', async (test) => {
        let expected = ' ~ޙ';
        let codepoints = [0x20, 0x7e, 0x799];
        let result = '';

        for (let value of codepoints) {
            result += codepointToUtf8(value);
        }

        test.same(result, expected);

        test.end();
    });

    test.test('javascriptEscapingEscapesOwaspRecommendedRanges', async (test) => {
        let immune = [',', '.', '_']; // Exceptions to escaping ranges

        for (let chr = 0; chr < 0xFF; ++chr) {
            if (chr >= 0x30 && chr <= 0x39
                || chr >= 0x41 && chr <= 0x5A
                || chr >= 0x61 && chr <= 0x7A) {
                let literal = codepointToUtf8(chr);

                test.same(await escape(getTemplate(), literal, 'js'), literal);
            } else {
                let literal = codepointToUtf8(chr);

                if (immune.includes(literal)) {
                    test.same(await escape(getTemplate(), literal, 'js'), literal);
                } else {
                    test.notSame(await escape(getTemplate(), literal, 'js'), literal);
                }
            }
        }

        test.end();
    });

    test.test('htmlAttributeEscapingEscapesOwaspRecommendedRanges', async (test) => {
        let immune = [',', '.', '-', '_']; // Exceptions to escaping ranges

        for (let chr = 0; chr < 0xFF; ++chr) {
            if (chr >= 0x30 && chr <= 0x39
                || chr >= 0x41 && chr <= 0x5A
                || chr >= 0x61 && chr <= 0x7A) {
                let literal = codepointToUtf8(chr);

                test.same(await escape(getTemplate(), literal, 'html_attr'), literal);
            } else {
                let literal = codepointToUtf8(chr);

                if (immune.includes(literal)) {
                    test.same(await escape(getTemplate(), literal, 'html_attr'), literal);
                } else {
                    test.notSame(await escape(getTemplate(), literal, 'html_attr'), literal);
                }
            }
        }

        test.end();
    });

    test.test('cssEscapingEscapesOwaspRecommendedRanges', async (test) => {
        for (let chr = 0; chr < 0xFF; ++chr) {
            if (chr >= 0x30 && chr <= 0x39
                || chr >= 0x41 && chr <= 0x5A
                || chr >= 0x61 && chr <= 0x7A) {
                let literal = codepointToUtf8(chr);

                test.same(await escape(getTemplate(), literal, 'css'), literal);
            } else {
                let literal = codepointToUtf8(chr);

                test.notSame(await escape(getTemplate(), literal, 'css'), literal);
            }
        }

        test.end();
    });

    test.test('customEscaper', async (test) => {
        let customEscaperCases: [string, string | number, string, string][] = [
            ['fooUTF-8', 'foo', 'foo', 'UTF-8'],
            ['UTF-8', null, 'foo', 'UTF-8'],
            ['42UTF-8', 42, 'foo', undefined],
        ];

        for (let customEscaperCase of customEscaperCases) {
            let template = getTemplate();

            template.environment.getCoreExtension().setEscaper('foo', foo_escaper_for_test);

            test.same(await escape(template, customEscaperCase[1], customEscaperCase[2], customEscaperCase[3]), customEscaperCase[0]);
        }

        test.end();
    });

    test.test('customUnknownEscaper', async (test) => {
        try {
            await escape(getTemplate(), 'foo', 'bar');

            test.fail();
        } catch (e) {
            test.same(e.message, 'Invalid escaping strategy "bar" (valid ones: html, js, url, css, html_attr).');
        }

        test.end();
    });

    test.end();
});
